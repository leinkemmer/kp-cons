#include <iostream>
#include <cmath>
using namespace std;

#include <boost/program_options.hpp>

#include "KP.hpp"

double sech(double x) {
    return 1/cosh(x);
}

int main(int argc, char* argv[]) {
    using namespace boost::program_options;

    string arg_string="";
    for(int i=0;i<argc;i++)
        arg_string += string(" ") + argv[i];
    cout << arg_string << endl;

    string problem, str_ls, spaced, str_nm, str_precond;
    int num_cells_x, num_cells_y, number_snapshots;
    double T, tau, epsilon, lambda, tol_linear, tol_newton;

    // WARNING: direct_evaluation can be unstable under certain circumstances
    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produces help message")
        ("snapshots",value<int>(&number_snapshots)->default_value(2),
         "number of snapshots that are written to disk (including initial and final time)")
        ("num_cells_x",value<int>(&num_cells_x)->default_value(512),
         "number of cells used in the space directions")
        ("num_cells_y",value<int>(&num_cells_y)->default_value(256),
         "number of cells used in the velocity directions")
        ("final_time,T",value<double>(&T)->default_value(0.4),
         "final time of the simulation")
        ("step_size",value<double>(&tau)->default_value(1e-3),
         "time step size used in the simulation")
        ("problem",value<string>(&problem)->default_value("soliton"),
         "initial value for the problem that is to be solved")
        ("epsilon",value<double>(&epsilon)->default_value(1.0),
         "epsilon parameter for the KP equation")
        ("lambda",value<double>(&lambda)->default_value(-1.0),
         "lambda parameter for the KP equation")
        ("ls",value<string>(&str_ls)->default_value("bicgstab"),
         "Linear solver used.")
        ("spaced",value<string>(&spaced)->default_value("cd4"),
         "Space discretization used.")
        ("nm",value<string>(&str_nm)->default_value("implicit"),
         "Numerical method used to solve Burgers' equation.")
        ("precond",value<string>(&str_precond)->default_value("default"),
         "Preconditioner used for iterative linear solvers.")
        ("tol_linear",value<double>(&tol_linear)->default_value(1e-14),
         "Tolerance used for linear solves.")
        ("tol_newton",value<double>(&tol_newton)->default_value(1e-14),
         "Tolerance used for the Newton iteration (nonlinear solve).");

    variables_map vm;
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);

    parameters::set_linear_tol(tol_linear);
    parameters::set_nonlinear_tol(tol_newton);

    if(vm.count("help")) {
        cout << desc << endl;
        return 1;
    } else {
        array<double,2> a, b;
        function<double(double,double)> u0;
        if(problem == "soliton") {
            a = {-20.0/epsilon, -20.0/epsilon};
            b = {20.0, 20.0};
            u0 = [epsilon](double x, double y) {
                double c = 2.0;
                double a = epsilon*sqrt(c)/sqrt(2.0);
                return c*pow(sech(a*x),2);
                return c;
            };
        } else {
            cout << "ERROR: Problem " << problem << " is not available (accepted values are soliton, schwartzian, zaitsev, and zaitsevpert)" << endl;
            exit(1);
        }

        KP kp(a, b, num_cells_x, num_cells_y, T, tau, u0, lambda, epsilon, number_snapshots, str_ls, spaced, str_nm, str_precond);
        kp.run();
    }

    return 0;
}

