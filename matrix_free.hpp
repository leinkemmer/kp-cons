#pragma once

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <fftw3.h>

class matrix_free;
using Eigen::SparseMatrix;

namespace Eigen {
    namespace internal {
        template<>
            struct traits<matrix_free> : public
                Eigen::internal::traits<Eigen::SparseMatrix<double>>
            {};
    }
}

struct diff_operator_minimal {
    virtual domain1d eval(const domain1d& v) = 0;
};

enum class matrix_free_type {
    implicit, semiimplicit
};

class matrix_free : public Eigen::EigenBase<matrix_free> {
public:
    typedef double Scalar;
    typedef double RealScalar;
    typedef int StorageIndex;
    enum {
        ColsAtCompileTime = Eigen::Dynamic,
        MaxColsAtCompileTime = Eigen::Dynamic,
        IsRowMajor = false
    };

    Index rows() const { return v.size(); }
    Index cols() const { return v.size(); }

    template<typename Rhs>
    Eigen::Product<matrix_free,Rhs,Eigen::AliasFreeProduct> operator*(const Eigen::MatrixBase<Rhs>& x) const {
        return Eigen::Product<matrix_free,Rhs,Eigen::AliasFreeProduct>(*this, x.derived());
    }

    // Custom API
    matrix_free(const Eigen::VectorXd& _v, diff_operator_minimal* _diff, double _alpha, double _tau, matrix_free_type _mft)
        : v(_v), diff(_diff), alpha(_alpha), tau(_tau), mft(_mft) {}

    Eigen::VectorXd v;
    diff_operator_minimal* diff;
    double alpha, tau;
    matrix_free_type mft;
};

namespace Eigen{
    namespace internal {

        template<typename Rhs>
        struct generic_product_impl<matrix_free, Rhs, SparseShape, DenseShape, GemvProduct> // GEMV stands for matrix-vector
        : generic_product_impl_base<matrix_free,Rhs,generic_product_impl<matrix_free,Rhs> >
        {
            typedef typename Product<matrix_free,Rhs>::Scalar Scalar;
            template<typename Dest>
                static void scaleAndAddTo(Dest& dst, const matrix_free& lhs, const Rhs& rhs, const Scalar& scale) {
                    // From the Eigen documentation:
                    // This method should implement "dst += alpha * lhs * rhs" inplace,
                    // however, for iterative solvers, alpha is always equal to 1, so let's not bother about it.
                    assert(scale==Scalar(1) && "scaling is not implemented");
                    
                    gt::time_it _t("matrix_free");

                    double alpha = lhs.alpha;
                    double tau   = lhs.tau;
                    diff_operator_minimal& diff = *lhs.diff;
                    const domain1d& v = lhs.v;

                    if(lhs.mft == matrix_free_type::implicit)
                        dst = 2.0*rhs + alpha*tau/3.0*(prod(diff.eval(v),rhs) + prod(v,diff.eval(rhs)) + 2.0*diff.eval(prod(v,rhs)));
                    else
                        dst = rhs + alpha*tau/6.0*(prod(v,diff.eval(rhs)) + diff.eval(prod(v,rhs)));
                }
        };
    }
}

// Based on the code for the diagonal preconditioner
// https://bitbucket.org/eigen/eigen/src/1a24287c6c133b46f8929cf5a4550e270ab66025/Eigen/src/IterativeLinearSolvers/BasicPreconditioners.h?at=default&fileviewer=file-view-default#BasicPreconditioners.h-185

// TODO: Eigen sees a preconditioner as a pure type (without external state information). The class gets
// a matrix and then constructs a preconditioner. However, this is not what we want to do in the matrix free
// case. So we use a global state which is certainly not optimal.

Eigen::SparseMatrix<double> global_matrix_precond;

struct banded_matrix_precond {

    typedef double   Scalar;
    typedef Eigen::VectorXd Vector;
    public:
    typedef typename Vector::StorageIndex StorageIndex;
    enum {
        ColsAtCompileTime    = Eigen::Dynamic,
        MaxColsAtCompileTime = Eigen::Dynamic
    };

    banded_matrix_precond() : m_isInitialized(true) {}

    explicit banded_matrix_precond(const Eigen::SparseMatrix<double>& mat) /*: matrix(mat)*/
    {
        m_isInitialized = true;
    }

    Eigen::Index rows() const { return global_matrix_precond.rows(); }
    Eigen::Index cols() const { return global_matrix_precond.cols(); }

    template<typename MatType>
    banded_matrix_precond& analyzePattern(const MatType& )
    {
        return *this;
    }

    template<typename MatType>
    banded_matrix_precond& factorize(const MatType& mat)
    {
        return *this;
    }

    template<typename MatType>
    banded_matrix_precond& compute(const MatType& mat)
    {
        return factorize(mat);
    }

    template<typename Rhs, typename Dest>
    void _solve_impl(const Rhs& b, Dest& x) const
    {
        // TODO: this has to be replaced with the banded solver
        x = Eigen::SparseLU<Eigen::SparseMatrix<double>>(global_matrix_precond).solve(b);
    }

    template<typename Rhs> inline const Eigen::Solve<banded_matrix_precond, Rhs>
    solve(const Eigen::MatrixBase<Rhs>& b) const
    {
        eigen_assert(m_isInitialized && "banded_matrix_precond is not initialized.");
        eigen_assert(global_matrix_precond.rows()==b.rows()
                && "banded_matrix_precond::solve(): invalid number of rows of the right hand side matrix b");
        return Eigen::Solve<banded_matrix_precond, Rhs>(*this, b.derived());
    }

    Eigen::ComputationInfo info() { return Eigen::Success; }

    protected:
    bool m_isInitialized;
};

