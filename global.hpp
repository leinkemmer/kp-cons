#pragma once

namespace statistics {
    void   update_newton(int its);
    double avg_newton_its();

    void   update_lsits(int its);
    double avg_lsits();

    void reset();
}

namespace parameters {
    void   set_linear_tol(double tol);
    double get_linear_tol();

    void   set_nonlinear_tol(double tol);
    double get_nonlinear_tol();
}
