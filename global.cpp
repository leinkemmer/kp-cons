#ifdef _OPENMP
#include <omp.h>
#endif

namespace statistics {
    long newton_its   = 0;
    long newton_calls = 0;

    bool is_master() {
        #ifdef _OPENMP
        if(omp_get_thread_num() != 0)
            return false;
        #endif

        return true;
    }

    void update_newton(int its) {
        if(is_master()) {
            newton_calls++;
            newton_its += its;
        }
    }

    double avg_newton_its() {
        return double(newton_its)/double(newton_calls);
    }

    long ls_its   = 0;
    long ls_calls = 0;

    void update_lsits(int its) {
        if(is_master()) {
            ls_calls++;
            ls_its += its;
        }
    }

    double avg_lsits() {
        return double(ls_its)/double(ls_calls);
    }

    void reset() {
        newton_its   = 0;
        newton_calls = 0;
    }
}

namespace parameters {
    double linear_tol    = 1e-14;
    double nonlinear_tol = 1e-14;

    void set_linear_tol(double tol) {
        linear_tol = tol;
    }

    double get_linear_tol() {
        return linear_tol;
    }

    void set_nonlinear_tol(double tol) {
        nonlinear_tol = tol;
    }

    double get_nonlinear_tol() {
        return nonlinear_tol;
    }
}

