#pragma once

#include <cmath>
#include <map>
#include <iostream>
using namespace std;

#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#ifdef _OPENMP
#include <omp.h>
#endif

/// This timer class measures the elapsed time between two events. Timers can be
/// started and stopped repeatedly. The total time as well as the average time
/// between two events can be queried using the total() and average() methods,
/// respectively.
struct timer {
    timespec t_start;
    bool running;
    double   elapsed;
    unsigned counter;
    double elapsed_sq;

    timer() {
        counter = 0;
        elapsed = 0.0;
        running = false;
        elapsed_sq = 0.0;
    }

    void reset() {
        counter = 0;
        elapsed = 0.0;
        running = false;
        elapsed_sq = 0.0;
    }

    void start() {
        clock_gettime(CLOCK_MONOTONIC, &t_start);
        running = true;
    }

    /// The stop method returns the elapsed time since the last call of start().
    double stop() {
        if(running == false) {
            cout << "WARNING: timer::stop() has been called without calling timer::start() first." << endl;
            return 0.0;
        } else {
            timespec t_end;
            clock_gettime(CLOCK_MONOTONIC, &t_end);
            int sec  = t_end.tv_sec-t_start.tv_sec;
            double nsec = ((double)(t_end.tv_nsec-t_start.tv_nsec));
            if(nsec < 0.0) {
                nsec += 1e9;
                sec--;
            }
            double t = (double)sec + nsec/1e9;
            counter++;
            elapsed += t;
            elapsed_sq += t*t;
            return t;
        }
    }

    double total() {
        return elapsed;
    }

    double average() {
        return elapsed/double(counter);
    }

    double deviation() {
        return sqrt(elapsed_sq/double(counter)-average()*average());
    }
};

namespace gt {
    map<string,timer> timers;

    bool is_master() {
        #ifdef _OPENMP
        if(omp_get_thread_num() != 0)
            return false;
        #endif

        return true;
    }
   
    void reset() {
        for(auto& el : timers)
            el.second.reset();
    }

    void print() {
        for(auto el : timers)
            cout << "gt " << el.first << ": " << el.second.total() << " s" << endl;
    }

    void start(string name) {
        if(is_master())
            timers[name].start();

    }

    void stop(string name) {
        if(is_master())
            timers[name].stop();
    }

    // to use as RAII
    struct time_it {
        time_it(string _name) : name(_name) {
            start(name);
        }

        ~time_it() {
            stop(name);
        }

        private:
        string name;
    };
}

