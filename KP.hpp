#pragma omp

#include <fstream>
using namespace std;

#include <boost/format.hpp>

#include <Eigen/Sparse>

#include "timer.hpp"
#include "global.hpp"
#include "domain.hpp"
#include "newton.hpp"
#include "matrix_free.hpp"


void apply_dispersion(double tau, domain2d_frequency& u, double lambda, double epsilon) {
    gt::time_it _t("dispersion");

    int Nx = u.Nx; int Ny = u.Ny;
    double Lx = u.Lx; double Ly = u.Ly;

    double delta = numeric_limits<double>::epsilon(); // machine epsilon
    for(int j=0;j<Ny;j++) {
        for(int i=0;i<Nx/2+1;i++) {
            double i_freq = i; // in this case there are no negative frequencies
            double j_freq = (j<Ny/2+1) ? j : -(Ny-j);
            i_freq *= 2*M_PI/Lx; j_freq *= 2*M_PI/Ly;

            if(i!=0) {
                complex<double> dispersion(0.0,-pow(i_freq,3));
                complex<double> regularized_x = 1.0/(pow(i_freq,2) + pow(lambda*delta,2))
                    *complex<double>(-lambda*delta, -i_freq);
                complex<double> diffusion_y(-pow(j_freq,2),0.0);
                complex<double> diffusion = regularized_x*diffusion_y;

                complex<double> exponent = -pow(epsilon,2)*dispersion - lambda*diffusion;

                fftw_complex &v = *(u.uf + i + j*(Nx/2+1));
                complex<double> value(v[0], v[1]);
                value *= exp(exponent*tau);
                v[0] = value.real()/double(Nx*Ny); v[1] = value.imag()/double(Nx*Ny);
            } else {
                fftw_complex &v = *(u.uf + i + j*(Nx/2+1));
                if(j != 0) {
                    v[0] = 0.0; v[1] = 0.0;
                } else {
                    v[0] /= double(Nx*Ny);
                    v[1] /= double(Nx*Ny);
                }
            }
        }
    }
}

struct diff_operator {
    virtual domain1d eval(const domain1d& v) const = 0;
    virtual Eigen::SparseMatrix<double> matrix() const = 0;
    virtual ~diff_operator() {}
};


int mod(int i, int n) {
    if(i>=0)
        return i%n;
    else
        return n + i%n;
}

struct diff_cd2 : diff_operator {

    diff_cd2(int Nx, double h) {
        typedef Eigen::Triplet<double> Tr;
        vector<Tr> tr;
        tr.reserve(3*Nx);

        double coeff = 1.0/(2.0*h);

        m.resize(Nx,Nx);
        for(int i=0;i<Nx;i++) {
            tr.push_back(Tr(i,mod(i+1,Nx),coeff));
            tr.push_back(Tr(i,mod(i-1,Nx),-coeff));
        }

        m.setFromTriplets(tr.begin(), tr.end());
    }

    domain1d eval(const domain1d& v) const {
        return matrix()*v;
    }

    Eigen::SparseMatrix<double> matrix() const {
        return m;
    }

    private:
    Eigen::SparseMatrix<double> m;
};

struct diff_cd4 : diff_operator {

    diff_cd4(int Nx, double h) {
        typedef Eigen::Triplet<double> Tr;
        vector<Tr> tr;
        tr.reserve(4*Nx);

        double coeff1 = 1.0/(12.0*h);
        double coeff2 = 2.0/(3.0*h);

        m.resize(Nx,Nx);
        for(int i=0;i<Nx;i++) {
            tr.push_back(Tr(i,mod(i+2,Nx),-coeff1));
            tr.push_back(Tr(i,mod(i+1,Nx), coeff2));
            tr.push_back(Tr(i,mod(i-1,Nx),-coeff2));
            tr.push_back(Tr(i,mod(i-2,Nx), coeff1));
        }

        m.setFromTriplets(tr.begin(), tr.end());
    }

    domain1d eval(const domain1d& v) const {
        return matrix()*v;
    }

    Eigen::SparseMatrix<double> matrix() const {
        return m;
    }

    private:
    Eigen::SparseMatrix<double> m;
};

struct diff_fft : diff_operator_minimal {
    diff_fft(int _Nx, double _Lx) : Nx(_Nx), Lx(_Lx), f(Nx) {
    }

    domain1d eval(const domain1d& u) {
        gt::time_it _t("diff_fft");

        f.in = u;
        f.transform();

        for(int i=0;i<Nx/2+1;i++) {
            double i_freq = i;
            i_freq *= 2*M_PI/Lx;

            f.out[i] *= complex<double>(0.0,i_freq)/double(Nx);
        }

        domain1d out(u);
        f.inv_transform();
        return f.in;
    }

    private:
    int Nx;
    double Lx;
    fft_1d f;

};

typedef function<void(double,domain1d&,const diff_operator&,func_linear_solve,diff_operator*)> func_nm;
typedef function<void(double,domain1d&,diff_operator_minimal&,func_linear_solve_fft,diff_operator*)> func_nm_fft;

SparseMatrix<double> get_J_implicit(double tau, double alpha, const diff_operator& diff, const domain1d& v) {
    using namespace Eigen;
    SparseMatrix<double> J(v.size(),v.size());
    J.setIdentity();
    J *= 2.0;
    J += alpha*tau/3.0*diff.eval(v).asDiagonal();
    J += alpha*tau/3.0*v.asDiagonal()*diff.matrix();
    J += 2.0*alpha*tau/3.0*diff.matrix()*v.asDiagonal();
    return J;
}

void solve_burgers_implicit(double tau, domain1d& u, const diff_operator& diff,
        func_linear_solve ls, diff_operator*) {

    double alpha = 6.0;

    // Save initial value
    domain1d u0 = u;

    // this is the predictor (the starting value for the Newton iteration)
    domain1d u12  = u0 - alpha*0.25*tau*diff.eval(prod(u0,u0));
    domain1d uhat = u0 - alpha*0.5*tau*diff.eval(prod(u12,u12));
    u = 0.5*(uhat + u0);

    // Newton with initial guess u.
    // Note that the Newton iteration is performed with
    // u^{1/2}.
    newton<Eigen::SparseMatrix<double>>(u, ls,
           [tau,&diff,&u0,alpha](const domain1d& v) {
               // Why do we have to assign here instead of just return
               domain1d r = 2*v - 2*u0 + alpha*tau/3.0*(prod(v,diff.eval(v)) + diff.eval(prod(v,v)));
               return r;
           },
           [tau,&diff,alpha](const domain1d& v) {
               gt::time_it _t("J");
               return get_J_implicit(tau, alpha, diff, v);
           });

    // The output in terms of u^{m+1} (not u^{m+1/2})
    u = 2*u - u0;
}

void solve_burgers_implicit_fft(double tau, domain1d& u, diff_operator_minimal& diff,
        func_linear_solve_fft ls, diff_operator* diff_precond) {

    double alpha = 6.0;

    // Save initial value
    domain1d u0 = u;

    // this is the predictor (the starting value for the Newton iteration)
    domain1d u12  = u0 - alpha*0.25*tau*diff.eval(prod(u0,u0));
    domain1d uhat = u0 - alpha*0.5*tau*diff.eval(prod(u12,u12));
    u = 0.5*(uhat + u0);

    // Newton with initial guess u.
    // Note that the Newton iteration is performed with
    // u^{1/2}.
    function<precond_type(const domain1d& v)> precond = [tau,alpha,diff_precond](const domain1d& v) {
        return get_J_implicit(tau, alpha, *diff_precond, v);
    };
    newton<matrix_free>(u, ls,
           [tau,&diff,&u0,alpha](const domain1d& v) {
               // Why do we have to assign here instead of just return
               domain1d r = 2*v - 2*u0 + alpha*tau/3.0*(prod(v,diff.eval(v)) + diff.eval(prod(v,v)));
               return r;
           },
           [tau,&diff,alpha](const domain1d& v) {
               gt::time_it _t("J");
               return matrix_free(v, &diff, alpha, tau, matrix_free_type::implicit);
           },
           (diff_precond==nullptr) ? nullptr : &precond);

    // The output in terms of u^{m+1} (not u^{m+1/2})
    u = 2*u - u0;
}

Eigen::SparseMatrix<double> get_A_semiimplicit(double tau, double alpha, const diff_operator& diff, const domain1d& uhat) {
        Eigen::SparseMatrix<double> A(uhat.size(),uhat.size());
        A.setIdentity();
        A += alpha*tau/6.0*uhat.asDiagonal()*diff.matrix();
        A += alpha*tau/6.0*diff.matrix()*uhat.asDiagonal();
        return A;
}

void solve_burgers_semiimplicit(double tau, domain1d& u, const diff_operator& diff,
        func_linear_solve ls, diff_operator*) {

    double alpha = 6.0;

    // Save initial value
    domain1d u0 = u;

    // u^hat
    domain1d uhat = u0 - alpha*0.25*tau*diff.eval(prod(u0,u0));

    // predictor
    domain1d u1     = u0 - alpha*0.5*tau*diff.eval(prod(uhat,uhat));
    domain1d u_pred = 0.5*(u1 + u0);

    // Note that the linear solve is conducted for u^{1/2}
    Eigen::SparseMatrix<double> A;
    {
        gt::time_it _t("A");
        A = get_A_semiimplicit(tau, alpha, diff, uhat);
    }
    u = ls(A, u, u_pred, nullptr);

    // The output in terms of u^{m+1} (not u^{m+1/2})
    u = 2*u - u0;
}

void solve_burgers_semiimplicit_fft(double tau, domain1d& u, diff_operator_minimal& diff,
        func_linear_solve_fft ls, diff_operator* diff_precond) {

    double alpha = 6.0;

    // Save initial value
    domain1d u0 = u;

    // u^hat
    domain1d uhat = u0 - alpha*0.25*tau*diff.eval(prod(u0,u0));

    // predictor
    domain1d u1     = u0 - alpha*0.5*tau*diff.eval(prod(uhat,uhat));
    domain1d u_pred = 0.5*(u1 + u0);

    // Note that the linear solve is conducted for u^{1/2}
    precond_type precond;
    if(diff_precond != nullptr)
        precond = get_A_semiimplicit(tau, alpha, *diff_precond, uhat);
    matrix_free mfj(uhat, &diff, alpha, tau, matrix_free_type::semiimplicit);
    u = ls(mfj, u, u_pred, (diff_precond==nullptr) ? nullptr : &precond);

    // The output in terms of u^{m+1} (not u^{m+1/2})
    u = 2*u - u0;
}

template<class func_nm, class func_linear_solve, class d_operator>
void apply_burgers(double tau, domain2d_equi& u, func_nm numerical_method,
        func_linear_solve ls, d_operator& diff, diff_operator* diff_precond) {

    for(int j=0;j<u.Ny;j++) {
        // For each y-slice we solve Burgers' equation
        domain1d slice = u.get_slice_y(j);
        numerical_method(tau, slice, diff, ls, diff_precond);
        u.write_slice_y(j, slice);
    }
}

struct KP {
    double T, tau, Lx, Ly;
    int snapshots, nx, ny;
    double lambda, epsilon;

    domain2d_equi      u;
    domain2d_frequency u_f;

    // standard solvers
    func_linear_solve ls;
    unique_ptr<diff_operator> diff;
    func_nm numerical_method;

    // matrix-free solvers
    bool use_fft;
    func_linear_solve_fft ls_fft;
    unique_ptr<diff_operator_minimal> diff_m;
    func_nm_fft numerical_method_fft;
    diff_operator* diff_precond;


    KP(array<double,2> _a, array<double,2> _b, int _nx, int _ny, double _T, double _tau,
            function<double(double,double)> u0, double _lambda, double _epsilon, int _snapshots,
            string str_ls, string str_spaced, string str_nm, string str_precond) :
        T(_T), tau(_tau), Lx(_b[0]-_a[0]), Ly(_b[1]-_a[1]), snapshots(_snapshots),
        nx(_nx), ny(_ny), lambda(_lambda), epsilon(_epsilon),
        u(_nx,_ny,_b[0]-_a[0],_b[1]-_a[1]), u_f(_nx,_ny,_b[0]-_a[0],_b[1]-_a[1]), diff_precond(nullptr) {

        u.init(u0);

        use_fft = false;

        if(str_spaced == "cd2")
            diff.reset( new diff_cd2(u.Nx,u.hx) );
        else if(str_spaced == "cd4")
            diff.reset( new diff_cd4(u.Nx,u.hx) );
        else if(str_spaced == "fft") {
            diff_m.reset( new diff_fft(u.Nx,u.Lx) );
            use_fft = true;
        } else {
            cout << "ERROR: space discretization " << str_spaced << " is not available." << endl;
            exit(1);
        }

        if(str_nm == "implicit") {
            if(!use_fft)
                numerical_method = solve_burgers_implicit;
            else
                numerical_method_fft = solve_burgers_implicit_fft;
        } else if(str_nm == "semiimplicit") {
            if(!use_fft)
                numerical_method = solve_burgers_semiimplicit;
            else
                numerical_method_fft = solve_burgers_semiimplicit_fft;
        } else {
            cout << "ERROR: numerical method for solving Burgers' equation " << str_nm << " is not available" << endl;
            exit(1);
        }

        if(!use_fft) {
            if(str_ls == "LU")
                ls = linear_solve_LU;
            else if(str_ls == "sparseLU")
                ls = linear_solve_sparseLU;
            else if(str_ls == "bicgstab")
                ls = linear_solve_bicgstab;
            else {
                cout << "ERROR: solver " << str_ls << " is not available." << endl;
                exit(1);
            }
        } else {
            if(str_ls == "bicgstab_fft")
                ls_fft = linear_solve_bicgstab_fft;
            else {
                cout << "ERROR: solver " << str_ls << " is not available for fft based discretization." << endl;
                exit(1);
            }
        }

        if(use_fft) {
            if(str_precond == "cd2")
                diff_precond = new diff_cd2(u.Nx, u.hx);
            else if(str_precond == "cd4")
                diff_precond = new diff_cd4(u.Nx, u.hx);
            else if(str_precond != "default") {
                cout << "ERROR: preconditioner " << str_precond << " is not available" << endl;
                exit(1);
            }
        } else {
            if(str_precond != "default") {
                cout << "ERROR: preconditioners are only available for fft based methods" << endl;
                exit(1);
            }
        }
    }

    ~KP() {
        if(diff_precond != nullptr)
            delete diff_precond;
    }

    void step(double tau) {
        // apply the diffusion
        fft(u, u_f);
        apply_dispersion(0.5*tau, u_f, lambda, epsilon);
        inv_fft(u_f, u);

        if(!use_fft)
            apply_burgers(tau, u, numerical_method, ls, *diff, diff_precond);
        else
            apply_burgers(tau, u, numerical_method_fft, ls_fft, *diff_m, diff_precond);

        // apply the diffusion
        fft(u, u_f);
        apply_dispersion(0.5*tau, u_f, lambda, epsilon);
        inv_fft(u_f, u);
    }

    void run() {
        ofstream fs_evolution("evolution.data");
        fs_evolution.precision(16);
        fs_evolution << "# t max_amplitude mass momentum" << endl;

        int n_steps=ceil(T/tau); double t=0.0;
        for(int i=0;i<n_steps+1;i++) {
            fs_evolution << t << '\t' << u.max_amplitude() << '\t' << u.mass() << '\t' << u.momentum() << '\t' << constraint(u) << endl;

            string fn = (boost::format("f-t%1%.data")%t).str();
            if(i % int(ceil(n_steps/double(snapshots-1))) == 0 || i==n_steps)
                u.write(fn);

            if(i < n_steps+1) { // last step is only for output
                if(T-t<tau) tau=T-t;

                gt::reset();
                statistics::reset();
                timer t_step; t_step.start();
                step(tau);
                t_step.stop();
                t+=tau;

                cout << "t=" << t << "\ttotal time: " << t_step.total() << "\tAvg Newton its: " << statistics::avg_newton_its() << "\tAvg LS its: " << statistics::avg_lsits() << endl;
                gt::print();
            }
        }
    }

};

