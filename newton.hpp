#pragma once

#include <functional>
using namespace std;

#include <Eigen/Dense>
#include <Eigen/SparseLU>
#include <Eigen/IterativeLinearSolvers>
#include <unsupported/Eigen/IterativeSolvers>

#include "timer.hpp"
#include "global.hpp"
#include "domain.hpp"
#include "matrix_free.hpp"

typedef SparseMatrix<double> precond_type;
typedef function<domain1d(const Eigen::SparseMatrix<double>&, const domain1d&, const domain1d&, precond_type*)> func_linear_solve;
typedef function<domain1d(const matrix_free&, const domain1d&, const domain1d&, precond_type*)> func_linear_solve_fft;


domain1d linear_solve_LU(const Eigen::SparseMatrix<double>& A, const domain1d& b,
                         const domain1d&, precond_type* = nullptr) {
    domain1d out(b);
    {
        gt::time_it _t("LU");
        out = Eigen::PartialPivLU<Eigen::MatrixXd>(A).solve(b);
    }
    return out;
}


domain1d linear_solve_sparseLU(const Eigen::SparseMatrix<double>& A, const domain1d& b,
                               const domain1d&, precond_type* = nullptr) {
    domain1d out(b);
    {
        gt::time_it _t("SparseLU");
        out = Eigen::SparseLU<Eigen::SparseMatrix<double>>(A).solve(b);
    }
    return out;
}

domain1d linear_solve_bicgstab(const Eigen::SparseMatrix<double>& A, const domain1d& b,
                               const domain1d& u0, precond_type* = nullptr) {
    double tol = parameters::get_linear_tol();

    domain1d out(b);
    {
        gt::time_it _t("bicgstab");
        typedef Eigen::DiagonalPreconditioner<double> Precond;
        //typedef Eigen::IncompleteLUT<double> Precond; // Does not seem to pay off
        Eigen::BiCGSTAB<Eigen::SparseMatrix<double>,Precond> solver(A);
        solver.setTolerance(tol);
        out = solver.solveWithGuess(b,u0);
        statistics::update_lsits(solver.iterations());
    }

    return out;
}

domain1d linear_solve_bicgstab_fft(const matrix_free& A, const domain1d& b,
                                   const domain1d& u0, precond_type* mat_precond) {
    double tol = parameters::get_linear_tol();

    domain1d out(b);
    {
        gt::time_it _t("bicgstab_fft");
        if(mat_precond == nullptr) {
            typedef Eigen::IdentityPreconditioner Precond;
            Eigen::BiCGSTAB<matrix_free,Precond> solver(A);
            solver.setTolerance(tol);
            out = solver.solveWithGuess(b,u0);
            statistics::update_lsits(solver.iterations());
        } else {
            // set up the preconditioner
            global_matrix_precond = *mat_precond;
            typedef banded_matrix_precond Precond;

            // solve the system
            Eigen::BiCGSTAB<matrix_free,Precond> solver(A);
            solver.setTolerance(tol);
            out = solver.solveWithGuess(b,u0);
            statistics::update_lsits(solver.iterations());
        }
    }

    return out;
}

template<class MatrixType, class func_linear_solve>
void newton(domain1d& v, func_linear_solve ls,
            function<domain1d(const domain1d&)> F,
            function<MatrixType(const domain1d&)> J,
            function<precond_type(const domain1d& v)>* J_precond = nullptr) {

    double tol = parameters::get_nonlinear_tol();

    domain1d delta(v);
    bool success = false;
    int j;
    for(j=0;j<100;j++) {
        if(J_precond == nullptr)
            delta = ls(J(v), F(v), Eigen::VectorXd::Zero(v.size()), nullptr);
        else {
            precond_type precond = (*J_precond)(v);
            delta = ls(J(v), F(v), Eigen::VectorXd::Zero(v.size()), &precond);
        }

        if(delta.norm() < tol) {
            success = true;
            break;
        }
        v -= delta;
    }

    if(!success) {
        cout << "ERROR: Newton's method did not convergence in 100 iterations." << endl;
        exit(1);
    }
    statistics::update_newton(j+1);
}

