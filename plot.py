#!/usr/bin/env python
from numpy import *
from pylab import *
import sys

if len(sys.argv) != 2:
    print("ERROR: please specify the file to plot as the single argument.")
    sys.exit(1)

subplot(2,3,1);

z=loadtxt(sys.argv[1])
nx=len(unique(z[:,0]))
ny=len(unique(z[:,1]))
z=z[np.lexsort((z[:,0],z[:,1]))]
z=z[:,2]
z=z.reshape((ny,nx))
title('solution')
imshow(z)
colorbar()

subplot(2,3,2)
title('slice')
data=z[int(ny/2),:]
plot(arange(0,len(data)),data)


data = loadtxt("evolution.data")

subplot(2,3,3)
title('amplitude')
plot(data[:,0],data[:,1])

subplot(2,3,4)
title('error in mass')
semilogy(data[:,0],abs(data[:,2]-data[0,2])/abs(data[0,2]))

subplot(2,3,5)
title('error in L^2 norm')
semilogy(data[:,0],abs(data[:,3]-data[0,3])/abs(data[0,3]))

show()
