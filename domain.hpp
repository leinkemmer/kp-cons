#pragma once

#include <numeric>
#include <Eigen/Dense>
#include <fftw3.h>

typedef Eigen::VectorXd domain1d;

domain1d prod(const domain1d& v1, const domain1d& v2) {
    domain1d r =  v1.array()*v2.array();
    return r;
}

struct fft_1d {
    fftw_plan p, p_inv;
    domain1d in;
    vector<complex<double>> out;

    fft_1d(int n) : in(n), out(n) {
        p     = fftw_plan_dft_r2c_1d(n, &in(0), (fftw_complex*)&out[0],
                                 FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
        p_inv = fftw_plan_dft_c2r_1d(n, (fftw_complex*)&out[0], &in(0), FFTW_ESTIMATE);
    }

    ~fft_1d() {
        fftw_destroy_plan(p);
        fftw_destroy_plan(p_inv);
    }

    void transform() {
        fftw_execute(p);
    }

    void inv_transform() {
        fftw_execute(p_inv);
    }
};

struct domain2d_equi {
    private:
        domain2d_equi() {}
    public:

        int Nx, Ny;
        double* un;
        double Lx, Ly;
        double hx, hy;

        domain2d_equi(int _Nx, int _Ny, double _Lx, double _Ly)
            : Nx(_Nx), Ny(_Ny), Lx(_Lx), Ly(_Ly) {

            hx = Lx/double(Nx); hy = Ly/double(Ny);
            un = fftw_alloc_real(Nx*Ny);
            if(un == NULL) {
                cout << "ERROR: could not initialize memory " << Nx << " " << Ny << endl;
                exit(1);
            }
        }

        ~domain2d_equi() {
            fftw_free(un);
        }

        double& operator()(int i, int j) {
            return un[i + j*Nx];
        }

        double x(int i) {
            // TODO: allow non-symmetric domains
            return -0.5*Lx + i*hx;
        }

        double y(int j) {
            // TODO: allow non-symmetric domains
            return -0.5*Ly + j*hy;
        }

        domain1d get_slice_y(int j) {
            domain1d ret(Nx);
            copy(un+j*Nx, un+(j+1)*Nx, &ret(0));
            return ret;
        }

        void write_slice_y(int j, const domain1d& slice) {
            copy(&slice[0], &slice[0]+Nx, un+j*Nx);
        }

        void init(function<double(double,double)> u0) {
            for(int j=0;j<Ny;j++)
                for(int i=0;i<Nx;i++)
                    (*this)(i,j) = u0(x(i),y(j));
        }

        void write(string fn) {
            ofstream fs(fn.c_str());
            fs.precision(16);
            for(int j=0;j<Ny;j++)
                for(int i=0;i<Nx;i++)
                    fs << x(i) << " " << y(j) << " " << (*this)(i,j) << endl;
            fs.close();
        }

        double max_amplitude() {
            return *max_element(un, un+Nx*Ny);
        }

        double mass() {
            return accumulate(un, un+Nx*Ny, 0.0)*hx*hy;
        }

        // momentum (L^2 norm squared)
        double momentum() {
            return inner_product(un, un+Nx*Ny, un, 0.0)*hx*hy;
        }

};


struct domain2d_frequency {

    int Nx, Ny;
    double Lx, Ly;
    double hx, hy;
    fftw_complex* uf;

    domain2d_frequency(int _Nx, int _Ny, double _Lx, double _Ly)
        : Nx(_Nx), Ny(_Ny), Lx(_Lx), Ly(_Ly) {
        uf = fftw_alloc_complex((Ny/2+1)*Nx);
        if(uf == NULL) {
            cout << "ERROR: could not initialize memory (frequency) " << Nx << " " << Ny << endl;
            exit(1);
        }
    }

    ~domain2d_frequency() {
        fftw_free(uf);
    }
};

void fft(const domain2d_equi& in, domain2d_frequency& out) {
    gt::time_it _t("fft");
    fftw_plan p = fftw_plan_dft_r2c_2d(in.Ny,in.Nx,in.un,out.uf,FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
    fftw_execute(p);
    fftw_destroy_plan(p);
}

void inv_fft(domain2d_frequency& in, domain2d_equi& out) {
    gt::time_it _t("fft");
    // Backward Fourier transform
    fftw_plan p = fftw_plan_dft_c2r_2d(in.Ny,in.Nx,in.uf,out.un,FFTW_ESTIMATE);
    fftw_execute(p);
    fftw_destroy_plan(p);
}

// max_y(|\int \partial_yy u dx|)
double constraint(const domain2d_equi& u) {

    domain2d_frequency u_f(u.Nx, u.Ny, u.Lx, u.Ly);
    domain2d_equi u_2(u.Nx, u.Ny, u.Lx, u.Ly);
    fft(u, u_f);
    for(int j=0;j<u.Ny;j++) {
        for(int i=0;i<u.Nx/2+1;i++) {
            double j_freq = (j<u.Ny/2+1) ? j : -(u.Ny-j);
            j_freq *= 2*M_PI/u.Ly;

            complex<double> d_yy(-pow(j_freq,2),0.0);

            fftw_complex &v = *(u_f.uf + i + j*(u.Nx/2+1));
            complex<double> value(v[0], v[1]);
            value *= d_yy;
            v[0] = value.real()/double(u.Nx*u.Ny); v[1] = value.imag()/double(u.Nx*u.Ny);
        }
    }
    inv_fft(u_f, u_2);

    double cstr_max = 0.0;
    for(int j=0;j<u.Ny;j++) {
        double int_x = 0.0;
        for(int i=0;i<u.Nx;i++)
            int_x += u.hx*u_2(i,j);
        cstr_max = max(cstr_max, int_x);
    }

    return cstr_max;
}

